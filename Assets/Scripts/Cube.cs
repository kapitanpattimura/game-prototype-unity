﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cube : MonoBehaviour {

    public List<GameObject> waypoints;    
    private int indexCurrentWaypoint;
	
	void Start () {
        if (waypoints.Count == 0) {
            Debug.Log("No waypoints!");
            return;
        }
        indexCurrentWaypoint = 0;
    }

    private IEnumerator MoveToWaypoint (GameObject waypoint, float time) {
        float elapsedTime = 0f;
        while (elapsedTime < time) {
            transform.position = Vector3.Lerp(transform.position, waypoints[indexCurrentWaypoint].transform.position, elapsedTime / time);
            elapsedTime += Time.deltaTime;
            yield return null;
        }
        SetNextWaypoint ();
    }
	
	void Update () {
        if (Input.GetKeyUp(KeyCode.M)) {
            if (waypoints[indexCurrentWaypoint] != null) {
                StartCoroutine(MoveToWaypoint(waypoints[indexCurrentWaypoint], 1f));
            }
        }        
	}

    private void SetNextWaypoint() {
        if (indexCurrentWaypoint < waypoints.Count - 1) {
            indexCurrentWaypoint++;
        } else {
            indexCurrentWaypoint = 0;
        }
    }

}
