﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCWaypoints : MonoBehaviour {

    public List<GameObject> waypoints;

    private int indexCurrentWaypoint;

	// Use this for initialization
	void Start () {
        indexCurrentWaypoint = 0;
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.M)) {
            // Start moving cube.
            //waypoints[indexCurrentWaypoint];
            StartCoroutine(MoveNPCToWaypoint (waypoints[indexCurrentWaypoint], 10f));
        }
	}

    private IEnumerator MoveNPCToWaypoint (GameObject targetWaypoint, float time) {
        float elapsedTime = 0f;

        Debug.Log("1 elapsedTime=" + elapsedTime);

        while (elapsedTime < time) {
            transform.position = Vector3.Lerp(transform.position, waypoints[indexCurrentWaypoint].transform.position, elapsedTime / time);
            elapsedTime += Time.deltaTime;
            Debug.Log("2 elapsedTime=" + elapsedTime);
            yield return null;
        }

        Debug.Log("3 elapsedTime=" + elapsedTime);

        //Debug.Log("Start to moving NPC...");
        //yield return new WaitForSeconds(1f);
        //Debug.Log("Finished!");
    }
}
