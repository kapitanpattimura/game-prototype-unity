﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public const int STATE_INIT = 0;
    public const int STATE_SPLASH = 1;
    public const int STATE_MENU = 2;

    private int currentState;

    private const int ACTION_INIT = 0;
    private const int ACTION_UPDATE = 1;

    public int currentAction;


    public static GameManager instance;

    private void Awake() {
        instance = this;
    }

    void Start () {
        UpdateState (STATE_INIT, ACTION_INIT);        
    }

    private void UpdateState (int state, int action) {
        currentState = state;
        currentAction = action;

        switch (state) {
            case STATE_INIT:
                Debug.Log("Initialize!");
                //UpdateState (STATE_SPLASH, STATE_INIT);
                break;

            case STATE_SPLASH:
                if (action == ACTION_INIT) {
                    Debug.Log("Show Splash!");
                    HUDManager.instance.screenSplash.SetActive(true);
                    currentAction = ACTION_UPDATE;

                } else if (action == ACTION_UPDATE)  {

                    // Show splash.
                    Debug.Log("Showing Splash...");

                    if (Input.GetKeyDown(KeyCode.Space)) {

                        UpdateState(STATE_MENU, STATE_INIT);
                    }

                }
                break;

            case STATE_MENU:
                if (currentAction == ACTION_INIT) {
                    HUDManager.instance.screenSplash.SetActive(false);
                    HUDManager.instance.screenMenu.SetActive(true);                    
                }
                Debug.Log("Showing Menu!");
                break;
        }        
    }
	
	// Update is called once per frame
	void Update () {
		if (currentAction == ACTION_UPDATE) {
            UpdateState (currentState, currentAction);
        }
	}

    public void OnButtonPlay() {
        Debug.Log("Play button pressed!");
        UpdateState(STATE_MENU, STATE_INIT);
    }

    public void OnButtonExit() {
        Debug.Log("Play button pressed!");
    }

}
